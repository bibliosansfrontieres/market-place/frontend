import { defineConfig } from 'cypress';
console.log(process.env.NODE_ENV);
const isDevEnv = process.env.NODE_ENV === 'development' ? true : false;

export default defineConfig({
  component: {
    devServer: {
      framework: 'next',
      bundler: 'webpack',
    },
  },

  e2e: {
    screenshotOnRunFailure: isDevEnv,
    video: isDevEnv,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
